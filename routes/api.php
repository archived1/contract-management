<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', [
    'namespace' => 'Auth',
    'middleware' => 'auth:api',
    'uses' => 'AuthController@user',
]);
Route::get('/address/cep', [
    'as' => 'api.address.cep',
    'uses' => 'ApiAddressController@cep'
]);
