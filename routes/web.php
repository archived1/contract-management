<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Voyager::routes();
Route::get('/documents/pdf/{id}', ['as' => 'document.pdf', 'uses' => 'ContractController@buildPDF']);
Route::get('/documents/docx/{id}', ['as' => 'document.docx', 'uses' => 'ContractController@buildDOCX']);
Route::group(['middleware' => ['admin.user']], function (){
    Route::get('/petition', [
        'as' => 'voyager.documents.petition',
        'uses' => 'ContractController@create'
    ]);
    Route::get('/contracts-models/json/{id}', 'ContractModelController@json');

    Route::group(['prefix' => 'documents'], function (){
        Route::get('send-email/{id}', [
            'as' => 'document.sendEmail',
            'uses' => 'ContractController@sendEmail'
        ]);

        Route::get('preview/{id}', [
            'as' => 'document.preview',
            'uses' => 'ContractController@buildPDF'
        ]);

        Route::get('download/{id}', [
            'as' => 'document.download_docx',
            'uses' => 'ContractController@buildDOCX'
        ]);
    });

    Route::group(['prefix' => 'timbre'], function (){
        Route::get('/', [
            'as' => 'timbre.index',
            'uses' => 'SettingDocumentsController@index'
        ]);

        Route::post('/', [
            'as' => 'timbre.update',
            'uses' => 'SettingDocumentsController@update'
        ]);
    });

    Route::group(['prefix' => 'customers'], function (){
        Route::get('download/{download}',[
            'as' => 'customer.download',
            'uses' => 'CustomerController@download'
        ]);

        Route::post('{customer}/uploads',[
            'as' => 'customer.upload',
            'uses' => 'CustomerController@upload'
        ]);

        Route::get('delete_upload/{document}',[
            'as' => 'customer.delete_upload',
            'uses' => 'CustomerController@deleteUpload'
        ]);
    });

    Route::group(['prefix' => 'reports'],function (){
        Route::get('/', [
            'as' => 'reports.index',
            'uses' => 'ReportController@index'
        ]);

        Route::post('/',[
           'as' => 'reports.export',
           'uses' => 'ReportController@export'
        ]);
    });
});
