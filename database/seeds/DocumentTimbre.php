<?php

use Illuminate\Database\Seeder;

class DocumentTimbre extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DocumentTimbre::create([
            'id' => 1,
            'name' => 'header',
            'path' => ''
        ]);

        DocumentTimbre::create([
            'id' => 2,
            'name' => 'footer',
            'path' => ''
        ]);
    }
}
