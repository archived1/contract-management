<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToLogDownloadContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('log_download_contracts', function (Blueprint $table) {
            $table->bigInteger('customer_id')->unsigned()->nullable()->after('contract_id');

            $table->foreign('customer_id')->references('id')->on('customers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_download_contracts', function (Blueprint $table) {
            $table->dropForeign('log_download_contracts_customer_id_foreign');
            $table->dropColumn('customer_id');
        });
    }
}
