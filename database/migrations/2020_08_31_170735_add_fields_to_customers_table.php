<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->string('company')->nullable()->after('name');
            $table->string('cnpj_cpf')->nullable()->after('company');
            $table->string('rg')->nullable()->after('cnpj_cpf');
            $table->string('ie')->nullable()->after('rg');
            $table->string('owner')->nullable()->after('ie');
            $table->date('date_of_birth')->nullable()->after('owner');
            $table->string('phone_1')->nullable()->after('email');
            $table->string('phone_2')->nullable()->after('phone_1');
            $table->string('address_street')->nullable()->after('phone_2');
            $table->string('address_n')->nullable()->after('address_street');
            $table->string('address_complement')->nullable()->after('address_n');
            $table->string('zip_code')->nullable()->after('address_complement');
            $table->string('district')->nullable()->after('zip_code');
            $table->string('city')->nullable()->after('district');
            $table->string('uf',2)->nullable()->after('city');
            $table->string('obs')->nullable()->after('uf');
            $table->smallInteger('customer_type')->default(1)->after('obs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->dropColumn('cnpj_cpf');
            $table->dropColumn('rg');
            $table->dropColumn('ie');
            $table->dropColumn('company');
            $table->dropColumn('owner');
            $table->dropColumn('date_of_birth');
            $table->dropColumn('phone_1');
            $table->dropColumn('phone_2');
            $table->dropColumn('address_street');
            $table->dropColumn('address_n');
            $table->dropColumn('address_complement');
            $table->dropColumn('zip_code');
            $table->dropColumn('district');
            $table->dropColumn('city');
            $table->dropColumn('uf');
            $table->dropColumn('obs');
            $table->dropColumn('customer_type');
        });
    }
}
