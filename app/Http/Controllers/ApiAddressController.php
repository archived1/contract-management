<?php

namespace App\Http\Controllers;

use \Canducci\Cep\Facades\Cep;
use Illuminate\Http\Request;

class ApiAddressController extends Controller
{
    public function cep(Request $request)
    {
        $request->validate([
           'cep' => 'required|digits:8'
        ]);

        $response = Cep::find($request->input('cep'));
        $data = $response->getCepModel();

        return response()->json(['status' => $response->isOk(), 'address' => $data]);
    }
}
