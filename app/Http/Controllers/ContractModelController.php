<?php

namespace App\Http\Controllers;

use App\ContractModel;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class ContractModelController extends VoyagerBaseController
{
    public function json($id)
    {
        $contractModel = ContractModel::find($id);
        $status = !empty($contractModel);
        $content = $status ? $contractModel->content : null;
        return ['status' => $status, 'content' => $content];
    }
}
