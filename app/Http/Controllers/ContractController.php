<?php

namespace App\Http\Controllers;

use Log;
use App\Customer;
use App\Contract;
use App\ContractType;
use App\ContractParty;
use App\LogDownloadContract;
use App\Notifications\NewContract;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Route;
use Illuminate\Database\Eloquent\SoftDeletes;
use TCG\Voyager\Events\BreadDataUpdated;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;
use Barryvdh\DomPDF\Facade as PDF;
use Mustache_Engine;

class ContractController extends VoyagerBaseController
{
    public function show(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $isSoftDeleted = false;

        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses_recursive($model))) {
                $model = $model->withTrashed();
            }
            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
                $model = $model->{$dataType->scope}();
            }
            $dataTypeContent = call_user_func([$model, 'findOrFail'], $id);
            if ($dataTypeContent->deleted_at) {
                $isSoftDeleted = true;
            }
        } else {
            // If Model doest exist, get data from table name
            $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
        }

        // Replace relationships' keys for labels and create READ links if a slug is provided.
        $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType, true);

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'read');

        // Check permission
        $this->authorize('read', $dataTypeContent);

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        // Eagerload Relations
        $this->eagerLoadRelations($dataTypeContent, $dataType, 'read', $isModelTranslatable);

        $contractParties = [];
        collect($dataTypeContent->parties)->prepend($dataTypeContent->customer)->map(function ($customer, $key) use(&$contractParties) {
            ++$key;
            $contractParties["cliente_{$key}_nome"] = $customer->name;
            $contractParties["cliente_{$key}_razao_social"] = $customer->company;
            $contractParties["cliente_{$key}_cnpj_cpf"] = $customer->cnpj_cpf;
            $contractParties["cliente_{$key}_rg"] = $customer->rg;
            $contractParties["cliente_{$key}_ie"] = $customer->ie;
            $contractParties["cliente_{$key}_proprietario"] = $customer->owner;
            $contractParties["cliente_{$key}_data_nascimento"] = $customer->date_of_birth;
            $contractParties["cliente_{$key}_email"] = $customer->email;
            $contractParties["cliente_{$key}_telefone_1"] = $customer->phone_1;
            $contractParties["cliente_{$key}_telefone_2"] = $customer->phone_2;
            $contractParties["cliente_{$key}_endereco_rua"] = $customer->address_street;
            $contractParties["cliente_{$key}_endereco_n"] = $customer->address_n;
            $contractParties["cliente_{$key}_bairro"] = $customer->district;
            $contractParties["cliente_{$key}_cep"] = $customer->zip_code;
            $contractParties["cliente_{$key}_cidade"] = $customer->city;
            $contractParties["cliente_{$key}_uf"] = $customer->uf;
        });

        $view = 'voyager::bread.read';

        if (view()->exists("voyager::$slug.read")) {
            $view = "voyager::$slug.read";
        }

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable', 'isSoftDeleted', 'contractParties'));
    }

    public function create(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        $dataTypeContent = (strlen($dataType->model_name) != 0)
            ? new $dataType->model_name()
            : false;

        foreach ($dataType->addRows as $key => $row) {
            $dataType->addRows[$key]['col_width'] = $row->details->width ?? 100;
        }

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'add');

        if($request->route()->getName() == 'voyager.documents.petition') {
            $dataType->addRows->map(function ($item) {
                if($item->field == 'contract_belongsto_contract_type_relationship') {
                    $details = $item->details;
                    $details->default = ContractType::PETITION;
                    $item->details = $details;
                }

                return $item;
            });
        }

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        // Eagerload Relations
        $this->eagerLoadRelations($dataTypeContent, $dataType, 'add', $isModelTranslatable);

        $contractParties = [];
        $customVariables = [];
        $customers = Customer::all()->map(function ($customer) {
            return ["id" => $customer->id, "text" => $customer->name, "selected" => false, "disabled" => false];
        });

        $view = "voyager::$slug.edit-add";

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable', 'customers', 'contractParties', 'customVariables'));
    }

    public function store(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        $request->request->add(['created_by' => Auth::user()->id]);

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->addRows)->validate();
        $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());
        $this->insertUpdateParties($request, $data);
        if($request->input('sendmail') == 1) {
            $contractUrl = route('contract.pdf', ['id' => Crypt::encrypt($data->id)]);
            $contractTypeName = !empty($data->contractType) ? $data->contractType->name : null;
            collect($data->parties)->prepend($data->customer)->map(function ($customer) use($contractUrl, $contractTypeName) {
                $customer->notify(new NewContract($contractUrl, $contractTypeName, $customer->name));
            });
        }

        event(new BreadDataAdded($dataType, $data));

        if (!$request->has('_tagging')) {
            if (auth()->user()->can('browse', $data)) {
                $redirect = redirect()->route("voyager.{$dataType->slug}.index");
            } else {
                $redirect = redirect()->back();
            }

            return $redirect->with([
                'message'    => __('voyager::generic.successfully_added_new')." {$dataType->getTranslatedAttribute('display_name_singular')}",
                'alert-type' => 'success',
            ]);
        } else {
            return response()->json(['success' => true, 'data' => $data]);
        }
    }

    public function edit(Request $request, $id)
    {
        $slug = $this->getSlug($request);
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses_recursive($model))) {
                $model = $model->withTrashed();
            }
            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
                $model = $model->{$dataType->scope}();
            }
            $dataTypeContent = call_user_func([$model, 'findOrFail'], $id);
        } else {
            // If Model doest exist, get data from table name
            $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
        }

        foreach ($dataType->editRows as $key => $row) {
            $dataType->editRows[$key]['col_width'] = isset($row->details->width) ? $row->details->width : 100;
        }

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'edit');

        // Check permission
        $this->authorize('edit', $dataTypeContent);

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        // Eagerload Relations
        $this->eagerLoadRelations($dataTypeContent, $dataType, 'edit', $isModelTranslatable);

        $contractParties = ContractParty::where(['contract_id' => $id])->get()->keyBy('customer_id')->toArray();
        $customVariables = [];
        $customers = Customer::all()->map(function ($customer) use(&$contractParties, &$customVariables, $dataTypeContent) {
            $customer = ["id" => $customer->id, "text" => $customer->name, "selected" => false, "disabled" => false];

            if(array_key_exists($customer['id'], $contractParties)) {
                $contractParties[$customer['id']] = $customer;
                $customer['disabled'] = true;
                array_push($customVariables, $customer);
            }

            if($dataTypeContent->customer_id == $customer['id']) {
                $customer['disabled'] = true;
                array_unshift($customVariables, $customer);
            }

            return $customer;
        });

        $view = "voyager::$slug.edit-add";

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable', 'customers', 'contractParties', 'customVariables'));
    }

    public function update(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Compatibility with Model binding.
        $id = $id instanceof \Illuminate\Database\Eloquent\Model ? $id->{$id->getKeyName()} : $id;

        $model = app($dataType->model_name);
        if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
            $model = $model->{$dataType->scope}();
        }
        if ($model && in_array(SoftDeletes::class, class_uses_recursive($model))) {
            $data = $model->withTrashed()->findOrFail($id);
        } else {
            $data = $model->findOrFail($id);
        }

        // Check permission
        $this->authorize('edit', $data);

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->editRows, $dataType->name, $id)->validate();
        $this->insertUpdateData($request, $slug, $dataType->editRows, $data);
        $this->insertUpdateParties($request, $data);

        event(new BreadDataUpdated($dataType, $data));

        if (auth()->user()->can('browse', app($dataType->model_name))) {
            $redirect = redirect()->route("voyager.{$dataType->slug}.index");
        } else {
            $redirect = redirect()->back();
        }

        return $redirect->with([
            'message'    => __('voyager::generic.successfully_updated')." {$dataType->getTranslatedAttribute('display_name_singular')}",
            'alert-type' => 'success',
        ]);
    }

    private function insertUpdateParties(Request $request, $contract)
    {
        try {
            $parties = $request->only('contract_party');
            $parties = empty($parties) ? [] : $parties['contract_party'];
            $contract->parties()->sync($parties);
        }catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }

    public function sendEmail($id)
    {
        try {
            $contract = Contract::find($id);
            $dataType = Voyager::model('DataType')->where('slug', '=', "documents")->first();
            if (auth()->user()->can('browse', app($dataType->model_name)))
                $redirect = redirect()->route("voyager.{$dataType->slug}.index");
            else
                $redirect = redirect()->back();

            if(empty($contract)) {
                return $redirect->with([
                    'message' => __('voyager::generic.failed_sending_mail'),
                    'alert-type' => 'error',
                ]);
            }

            $contractUrl['PDF'] = route('document.pdf', ['id' => Crypt::encrypt($contract->id)]);
            $contractUrl['DOCX'] = route('document.docx', ['id' => Crypt::encrypt($contract->id)]);
            $contractTypeName = !empty($contract->contractType) ? $contract->contractType->name : null;
            collect($contract->parties)->prepend($contract->customer)->map(function ($customer) use($contractUrl, $contractTypeName) {
                $customer->notify(new NewContract($contractUrl, $contractTypeName, $customer->name));
            });

            return $redirect->with([
                'message' => __('voyager::generic.successfully_sending_mail'),
                'alert-type' => 'success'
            ]);
        }catch (\Exception $e) {
            return $redirect->with([
                'message' => __('voyager::generic.failed_sending_mail'),
                'alert-type' => 'error'
            ]);
        }
    }

    public function buildPDF(Request $request, $id)
    {
        try {
            if (Route::currentRouteName() == 'document.pdf') {
                $id = Crypt::decrypt($id);
            }

            $contract = Contract::find($id);
            if(empty($contract))
                return abort(404);

            if (Route::currentRouteName() == 'document.pdf') {
                LogDownloadContract::create(['contract_id' => $id, 'customer_id' => $contract->customer_id]);
            }

            $contractParties = [];
            collect($contract->parties)->prepend($contract->customer)->map(function ($customer, $key) use(&$contractParties) {
                ++$key;
                $contractParties["cliente_{$key}_nome"] = $customer->name;
                $contractParties["cliente_{$key}_razao_social"] = $customer->company;
                $contractParties["cliente_{$key}_cnpj_cpf"] = $customer->cnpj_cpf;
                $contractParties["cliente_{$key}_rg"] = $customer->rg;
                $contractParties["cliente_{$key}_ie"] = $customer->ie;
                $contractParties["cliente_{$key}_proprietario"] = $customer->owner;
                $contractParties["cliente_{$key}_data_nascimento"] = $customer->date_of_birth;
                $contractParties["cliente_{$key}_email"] = $customer->email;
                $contractParties["cliente_{$key}_telefone_1"] = $customer->phone_1;
                $contractParties["cliente_{$key}_telefone_2"] = $customer->phone_2;
                $contractParties["cliente_{$key}_endereco_rua"] = $customer->address_street;
                $contractParties["cliente_{$key}_endereco_n"] = $customer->address_n;
                $contractParties["cliente_{$key}_bairro"] = $customer->district;
                $contractParties["cliente_{$key}_cep"] = $customer->zip_code;
                $contractParties["cliente_{$key}_cidade"] = $customer->city;
                $contractParties["cliente_{$key}_uf"] = $customer->uf;
            });

            $m = new Mustache_Engine;
            $content = $m->render($contract->content, $contractParties);
            $timbre_header = view('docs.timbre_header')->render();
            $timbre_footer = view('docs.timbre_footer')->render();
            return PDF::loadView('docs.contractPDF', compact('content','timbre_header','timbre_footer'))->stream();
        }catch (\Exception $e) {
            return abort(404);
        }
    }

    public function buildDOCX(Request $request, $id)
    {
        try {
            if (Route::currentRouteName() == 'document.docx') {
                $id = Crypt::decrypt($id);
            }

            $contract = Contract::find($id);
            if(empty($contract))
                return abort(404);

            if (Route::currentRouteName() == 'document.docx') {
                LogDownloadContract::create(['contract_id' => $id, 'customer_id' => $contract->customer_id]);
            }

            $contractParties = [];
            collect($contract->parties)->prepend($contract->customer)->map(function ($customer, $key) use(&$contractParties) {
                ++$key;
                $contractParties["cliente_{$key}_nome"] = $customer->name;
                $contractParties["cliente_{$key}_razao_social"] = $customer->company;
                $contractParties["cliente_{$key}_cnpj_cpf"] = $customer->cnpj_cpf;
                $contractParties["cliente_{$key}_rg"] = $customer->rg;
                $contractParties["cliente_{$key}_ie"] = $customer->ie;
                $contractParties["cliente_{$key}_proprietario"] = $customer->owner;
                $contractParties["cliente_{$key}_data_nascimento"] = $customer->date_of_birth;
                $contractParties["cliente_{$key}_email"] = $customer->email;
                $contractParties["cliente_{$key}_telefone_1"] = $customer->phone_1;
                $contractParties["cliente_{$key}_telefone_2"] = $customer->phone_2;
                $contractParties["cliente_{$key}_endereco_rua"] = $customer->address_street;
                $contractParties["cliente_{$key}_endereco_n"] = $customer->address_n;
                $contractParties["cliente_{$key}_bairro"] = $customer->district;
                $contractParties["cliente_{$key}_cep"] = $customer->zip_code;
                $contractParties["cliente_{$key}_cidade"] = $customer->city;
                $contractParties["cliente_{$key}_uf"] = $customer->uf;
            });

            $m = new Mustache_Engine;
            $content = $m->render($contract->content, $contractParties);

            $phpWord = new \PhpOffice\PhpWord\PhpWord();
            $section = $phpWord->addSection();
            \PhpOffice\PhpWord\Shared\Html::addHtml($section, $content);
            $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
            $doc_name = "document{$id}.docx";
            $objWriter->save($doc_name);
            return response()->download($doc_name)->deleteFileAfterSend(true);
        }catch (\Exception $e) {
            return abort(404);
        }
    }
}
