<?php

namespace App\Http\Controllers;

use App\ContractType;
use App\Customer;
use App\DocumentsUpload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class CustomerController extends VoyagerBaseController
{
    public function show(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $isSoftDeleted = false;

        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses_recursive($model))) {
                $model = $model->withTrashed();
            }
            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
                $model = $model->{$dataType->scope}();
            }
            $dataTypeContent = call_user_func([$model, 'findOrFail'], $id);
            if ($dataTypeContent->deleted_at) {
                $isSoftDeleted = true;
            }
        } else {
            // If Model doest exist, get data from table name
            $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
        }

        // Replace relationships' keys for labels and create READ links if a slug is provided.
        $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType, true);

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'read');

        // Check permission
        $this->authorize('read', $dataTypeContent);

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        // Eagerload Relations
        $this->eagerLoadRelations($dataTypeContent, $dataType, 'read', $isModelTranslatable);

        $view = 'voyager::bread.read';

        if (view()->exists("voyager::$slug.read")) {
            $view = "voyager::$slug.read";
        }

        $documentTypers = ContractType::all();

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable', 'isSoftDeleted', 'documentTypers'));
    }

    public function download(Request $request, DocumentsUpload $download)
    {
        return response()->download(storage_path("app/customer/docs/$download->path"));
    }

    public function upload(Request $request, Customer $customer)
    {
        try {
            $request->validate([
                'document_name' => 'required|string|min:3|max:255',
                'document_type' => 'exists:contract_types,id',
                'document_file' => 'mimes:jpeg,bmp,png,gif,pdf'
            ]);

            $document_file = $request->file('document_file');
            $document_file_name = sha1($request->input('document_name') . date('Y-m-d H:i:s'));
            $document_file_name = "{$document_file_name}.{$document_file->getClientOriginalExtension()}";

            if($document_file->move(storage_path('app/customer/docs'), $document_file_name)) {
                $customer->uploads()->create([
                    'name' => $request->input('document_name'),
                    'path' => $document_file_name,
                    'type_id' => $request->input('document_type')
                ]);
            }

            return back()->with([
                'message'    => __('voyager::generic.successfully_added_new'),
                'alert-type' => 'success',
            ]);
        }catch (\Exception $e) {
            return back()->with([
                'message'    => __('voyager::generic.failed_added_new'),
                'alert-type' => 'error',
            ]);
        }
    }

    public function deleteUpload(Request $request, DocumentsUpload $document)
    {
        if($document->delete()) {
            Storage::delete("customer/docs/$document->path");
            return back()->with([
                'message'    => __('voyager::generic.deleted_success'),
                'alert-type' => 'success',
            ]);
        }
        else {
            return back()->with([
                'message'    => __('voyager::generic.deleted_fail'),
                'alert-type' => 'error',
            ]);
        }
    }
}
