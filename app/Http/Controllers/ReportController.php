<?php

namespace App\Http\Controllers;

use App\User;
use App\Customer;
use App\Exports\CustomerBirthdayExport;
use App\Exports\DocumentsDownloadsExport;
use App\Exports\DocumentsExport;
use Maatwebsite\Excel\Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ReportController extends Controller
{
    public function index()
    {
        $customers = Customer::all();
        $users = User::all();
        return view('reports.index', ['customers' => $customers, 'users' => $users]);
    }

    public function export(Excel $excel, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'date_start' => 'date_format:Y-m-d',
            'date_end' => 'date_format:Y-m-d',
            'month' => 'numeric',
            'user' => 'exists:users,id',
            'customer' => 'exists:customers,id'
        ]);

        if($validator->fails()) {
            return back()->with([
                'message' => implode(",", $validator->messages()->all()),
                'alert-type' => 'error',
            ]);
        }
        $filter = $validator->validated();
        $export = null;

        switch ($request->input('report_type')) {
            case 1:
            case 2:
            case 3:
                $export = new DocumentsExport($filter);
                break;
            case 4:
            case 5:
                $export = new DocumentsDownloadsExport($filter);
                break;
            case 6:
                $export = new CustomerBirthdayExport($filter);
                break;
            default:
                return back();
        }

        return $excel->download($export, 'relatório.xls',Excel::XLS);
    }
}
