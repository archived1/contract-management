<?php

namespace App\Http\Controllers;

use Artisan;
use Illuminate\Http\Request;

class SettingDocumentsController extends Controller
{
    public function index()
    {
        return view('timbre.index');
    }

    public function update(Request $request)
    {
        try {
            if($request->file('timbre_header')) {
                $timbre_header = $request->file('timbre_header');
                $timbre_header->move(storage_path('app/public/timbre'),'timbre-header.png');
            }

            if($request->file('timbre_footer')) {
                $timbre_header = $request->file('timbre_footer');
                $timbre_header->move(storage_path('app/public/timbre'),'timbre-footer.png');
            }

            Artisan::call('cache:clear');

            return redirect()->route("timbre.index")->with([
                'message'    => __('voyager::generic.successfully_updated'),
                'alert-type' => 'success',
            ]);
        } catch (\Exception $exception) {
            return back()->with([
                'message'    => __('voyager::generic.updated_fail'),
                'alert-type' => 'error',
            ]);
        }
    }
}
