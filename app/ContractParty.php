<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Facades\Voyager;

class ContractParty extends Model
{
    protected $fillable = ['contract_id', 'customer_id'];

    public function contract()
    {
        return $this->belongsTo('App\Contract', 'contract_id');
    }

    public function customer()
    {
        return $this->belongsTo('App\Customer','customer_id');
    }
}
