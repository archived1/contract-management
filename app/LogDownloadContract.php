<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogDownloadContract extends Model
{
    protected $fillable = [
        'contract_id',
        'customer_id'
    ];
}
