<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContractType extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name'
    ];

    const PETITION = 2;

    public function documents()
    {
        return $this->hasMany('App\Contract', 'customer_id');
    }

    public function uploads()
    {
        return $this->hasMany('App\DocumentsUpload','customer_id');
    }
}
