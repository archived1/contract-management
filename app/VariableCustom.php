<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VariableCustom extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'contract_type'
    ];
}
