<?php

namespace App\Exports;

use App\LogDownloadContract;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class DocumentsDownloadsExport implements FromCollection, WithHeadings
{
    private $date_start;
    private $date_end;
    private $customer;

    public function __construct(Array $filter)
    {
        $this->date_start   = $filter['date_start'] ?? null;
        $this->date_end     = $filter['date_end'] ?? null;
        $this->customer     = $filter['customer'] ?? null;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return LogDownloadContract::leftJoin('contracts', function ($join){
            $join->on('contracts.id', '=', 'log_download_contracts.contract_id');
        })->leftJoin('contract_types', function ($join) {
            $join->on('contract_types.id', '=', 'contracts.type_id');
        })->leftJoin('customers', function ($join) {
            $join->on('customers.id', '=', 'log_download_contracts.customer_id');
        })->when($this->date_start, function ($query) {
            return $query->where('log_download_contracts.created_at', '>=', $this->date_start);
        })->when($this->date_end, function ($query){
            return $query->where('log_download_contracts.created_at', '<=', $this->date_end);
        })->when($this->customer, function ($query){
            return $query->where('log_download_contracts.customer_id', '=', $this->customer);
        })->select(
            'log_download_contracts.contract_id',
            'contract_types.name as document_type',
            'customers.name as customer',
            'contracts.created_at'
        )->get();
    }

    public function headings(): array
    {
        return [
            'ID DO DOCUMENTO',
            'TIPO DE DOCUMENTO',
            'CLIENTE',
            'BAIXADO EM'
        ];
    }
}
