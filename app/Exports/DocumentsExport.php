<?php

namespace App\Exports;

use App\Contract;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class DocumentsExport implements FromCollection, WithHeadings
{
    private $date_start;
    private $date_end;
    private $customer;
    private $user;

    public function __construct(Array $filter)
    {
        $this->date_start   = $filter['date_start'] ?? null;
        $this->date_end     = $filter['date_end'] ?? null;
        $this->customer     = $filter['customer'] ?? null;
        $this->user         = $filter['user'] ?? null;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Contract::leftJoin('contract_types', function ($join) {
            $join->on('contract_types.id', '=', 'contracts.type_id');
        })->leftJoin('contract_models', function ($join) {
            $join->on('contract_models.id', '=', 'contracts.model_id');
        })->leftJoin('customers', function ($join) {
            $join->on('customers.id', '=', 'contracts.customer_id');
        })->leftJoin('users', function ($join){
            $join->on('users.id','=','contracts.created_by');
        })->when($this->date_start, function ($query){
            return $query->where('contracts.created_at', '>=', $this->date_start);
        })->when($this->date_end, function ($query){
            return $query->where('contracts.created_at', '<=', $this->date_end);
        })->when($this->customer, function ($query){
            return $query->where('contracts.customer_id', '=', $this->customer);
        })->when($this->user, function ($query){
                return $query->where('contracts.created_by', '=', $this->user);
        })->select(
            'contracts.id',
            'contract_types.name as document_type',
            'contract_models.name as document_model',
            'customers.name as customer',
            'users.name as created_by',
            'contracts.created_at'
        )->get();
    }

    public function headings(): array
    {
        return [
            'ID',
            'TIPO DE DOCUMENTO',
            'MODELO DE DOCUMENTO',
            'CLIENTE',
            'CRIADO POR',
            'CRIADO EM',
        ];
    }
}
