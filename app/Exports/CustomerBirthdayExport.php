<?php

namespace App\Exports;

use App\Customer;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CustomerBirthdayExport implements FromCollection, WithHeadings
{
    private $month;

    public function __construct(Array $filter)
    {
        $this->month = $filter['month'] ?? null;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Customer::select(
            'id',
            'name',
            'company',
            'cnpj_cpf',
            'rg',
            'ie',
            'owner',
            'date_of_birth',
            'email',
            'phone_1',
            'phone_2',
            'address_street',
            'address_n',
            'address_complement',
            'district',
            'zip_code',
            'city',
            'uf',
            'obs'
        )->when($this->month, function ($query){
            return $query->whereMonth('date_of_birth', $this->month);
        })->get();
    }

    public function headings(): array
    {
        return [
            'ID',
            'NOME',
            'RAZÃO SOCIAL',
            'CNPJ/CPF',
            'RG',
            'IE',
            'PROPRIETÁRIO',
            'DATA DE NASCIMENTO',
            'E-MAIL',
            'TELEFONE 1',
            'TELEFONE 2',
            'RUA',
            'Nº',
            'COMPLEMENTO',
            'BAIRRO',
            'CEP',
            'CIDADE',
            'UF',
            'OBS'
        ];
    }
}
