<?php

namespace App\Widgets;

use App\Customer;
use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Str;

class Birthday extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $count = Customer::whereMonth('date_of_birth', '=', date('m'))->count();
        $string = __('widgets.birthday');

        return view('widgets.generic', [
            'icon'   => 'voyager-gift',
            'title'  => "{$count} {$string}",
            'text'   => __('widgets.birthday_text', ['count' => $count, 'string' => Str::lower($string)]),
            'image' => asset('assets/img/widgets/birthday.jpg'),
        ]);
    }

    /**
     * Determine if the widget should be displayed.
     *
     * @return bool
     */
    public function shouldBeDisplayed()
    {
        return true;
    }
}
