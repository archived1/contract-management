<?php

namespace App\Widgets;

use App\Customer as Customers;
use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Str;

class Customer extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $count = Customers::count();
        $string = __('widgets.customer');

        return view('widgets.generic', [
            'icon'   => 'voyager-people',
            'title'  => "{$count} {$string}",
            'text'   => __('widgets.customer_text', ['count' => $count, 'string' => Str::lower($string)]),
            'button' => [
                'text' => __('widgets.customer_link_text'),
                'link' => route('voyager.customers.index'),
            ],
            'image' => asset('assets/img/widgets/customers.jpg'),
        ]);
    }

    /**
     * Determine if the widget should be displayed.
     *
     * @return bool
     */
    public function shouldBeDisplayed()
    {
        return true;
    }
}
