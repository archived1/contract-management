<?php

namespace App\Widgets;

use App\Contract;
use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Str;

class Document extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $count = Contract::count();
        $string = __('widgets.document');

        return view('widgets.generic', [
            'icon'   => 'voyager-documentation',
            'title'  => "{$count} {$string}",
            'text'   => __('widgets.document_text', ['count' => $count, 'string' => Str::lower($string)]),
            'button' => [
                'text' => __('widgets.document_link_text'),
                'link' => route('voyager.documents.index'),
            ],
            'image' => asset('assets/img/widgets/documents.jpg'),
        ]);
    }

    /**
     * Determine if the widget should be displayed.
     *
     * @return bool
     */
    public function shouldBeDisplayed()
    {
        return true;
    }
}
