<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Customer extends Model
{
    use SoftDeletes;
    use Notifiable;

    protected $fillable = [
        'name',
        'cnpj_cpf',
        'rg',
        'ie',
        'company',
        'owner',
        'date_of_birth',
        'avatar',
        'email',
        'phone_1',
        'phone_2',
        'address_street',
        'address_n',
        'address_complement',
        'zip_code',
        'district',
        'city',
        'uf',
        'obs'
    ];

    const PESSOA_FISICA = 1;
    const PESSOA_JURIDICA = 2;

    public function getAvatarAttribute($value)
    {
        return $value ?? config('voyager.user.default_avatar', 'users/default.png');
    }

    public function documents()
    {
        return $this->hasMany('App\Contract', 'customer_id');
    }

    public function uploads()
    {
        return $this->hasMany('App\DocumentsUpload','customer_id');
    }
}
