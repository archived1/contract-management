<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NewContract extends Notification
{
    use Queueable;

    private $contractUrl;
    private $contractTypeName;
    private $customerName;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($contractUrl, $contractTypeName, $customerName)
    {
        $this->contractUrl = $contractUrl;
        $this->contractTypeName = $contractTypeName;
        $this->customerName = $customerName;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $appName = setting('admin.name');
        return (new MailMessage)
                    ->subject("{$appName} - {$this->contractTypeName}")
                    ->markdown('mail.contract.send', [
                        'customerName' => $this->customerName,
                        'contractType' => $this->contractTypeName,
                        'urlPDF' => $this->contractUrl['PDF'],
                        'urlDOCX' => $this->contractUrl['DOCX']
                    ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
