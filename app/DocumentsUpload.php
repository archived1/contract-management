<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentsUpload extends Model
{
    protected $fillable = [
        'name',
        'path',
        'type_id',
        'customer_id'
    ];

    public function contractType()
    {
        return $this->belongsTo('App\ContractType','type_id');
    }

    public function customer()
    {
        return $this->belongsTo('App\Customer','customer_id');
    }
}
