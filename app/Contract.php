<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contract extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'type_id',
        'model_id',
        'customer_id',
        'content',
        'created_by'
    ];

    public function customer()
    {
        return $this->belongsTo('App\Customer','customer_id');
    }

    public function parties()
    {
        return $this->belongsToMany(
            'App\Customer',
            'contract_parties',
            'contract_id',
            'customer_id'
        );
    }

    public function contractType()
    {
        return $this->belongsTo('App\ContractType','type_id');
    }

    public function contractModel()
    {
        return $this->belongsTo('App\ContractModel','model_id');
    }
}
