<?php

return [
    // widget document
    'document'              => 'Documents',
    'document_link_text'    => 'View all documents',
    'document_text'         => 'You have :count :string in your database.',

    // widget customer
    'customer'              => 'Customers',
    'customer_link_text'    => 'View all customers',
    'customer_text'         => 'You have :count :string in your database.',

    // widget birthday person
    'birthday'              => 'Birthday Persons',
    'birthday_link_text'    => 'View all birthday persons',
    'birthday_text'         => 'You have :count :string in your database.'
];
