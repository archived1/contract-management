<?php

return [
    // widget documentos
    'document'              => 'Documentos',
    'document_link_text'    => 'Ver todos os documentos',
    'document_text'         => 'Tem :count :string no seu banco de dados.',

    // widget clientes
    'customer'              => 'Clientes',
    'customer_link_text'    => 'Ver todos os clientes',
    'customer_text'         => 'Tem :count :string no seu banco de dados.',

    // widget aniversariante
    'birthday'              => 'Aniversariantes',
    'birthday_link_text'    => 'Ver todos os aniversariantes',
    'birthday_text'         => 'Tem :count :string no seu banco de dados.'
];
