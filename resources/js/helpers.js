/**
 * Returns the base URL with the current segment and, if passed, append another segment path.
 * To rewrite the current segment path, set the second parameter to true
 *
 * @param {string} [segment_path]
 * @param {boolean} [replace]
 * @returns {string}
 */
window.url = function(segment_path, replace) {
    segment_path = _.isEmpty(segment_path) ? '' : segment_path;
    replace = _.isEmpty(replace) ? false : replace;

    var url = window.location.href,
        last_separator = n = url.lastIndexOf("/");

    if(replace || ++(last_separator) == url.length)
        url = url.substring(0,n);

    url += '/';
    url += segment_path;
    return url;
}

/**
 * Return the base URL with, if passed, the segment path
 *
 * @param {string} [segment_path]
 * @returns {string}
 */
window.base_url = function(segment_path) {
    segment_path = _.isEmpty(segment_path) ? '' : segment_path;
    var url  = document.location.origin;

    url += '/';
    url += segment_path;
    return url;
}

/**
 * Redirect the browser to the url passed
 *
 * @param {string} url
 * @returns {undefined}
 */
window.redirect = function(url) {
    window.location.href = url;
}
