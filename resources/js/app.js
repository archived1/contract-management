require('./bootstrap');
require('./helpers');


var MaskBehavior = function (val) {
    return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
};
var phoneOptions = {
    onKeyPress: function(val, e, field, options) {
        field.mask(SPMaskBehavior.apply({}, arguments), options);
    }
};
$('.phone').mask(MaskBehavior, phoneOptions);
var cpfCnpjOptions = {
    onKeyPress : function(cpf_cnpj, e, field, options) {
        var masks = ['000.000.000-009', '00.000.000/0000-00'];
        var mask = (cpf_cnpj.length > 14) ? masks[1] : masks[0];
        $('.cpf_cnpj').mask(mask, options);
    }
};
$('.cpf_cnpj').mask('000.000.000-009', cpfCnpjOptions);
$('.money').mask("#.##0,00", {reverse: true});
$('.date').mask('00/00/0000');
$('.cep').mask('00000-000');


