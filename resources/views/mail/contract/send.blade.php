@component('mail::message')
# Olá, {{$customerName}}

Segue o link do {{$contractType}} para download.

@component('mail::button', ['url' => $urlPDF])
Download PDF
@endcomponent
@component('mail::button', ['url' => $urlDOCX])
    Download DOCX
@endcomponent

Qualquer dúdivas, estamos a disposição.
@endcomponent
