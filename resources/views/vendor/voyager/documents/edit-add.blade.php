@php
    $edit = !is_null($dataTypeContent->getKey());
    $add  = is_null($dataTypeContent->getKey());
@endphp

@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular'))

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular') }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form"
                            class="form-edit-add"
                            action="{{ $edit ? route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) : route('voyager.'.$dataType->slug.'.store') }}"
                            method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                        @if($edit)
                            {{ method_field("PUT") }}
                        @endif

                        <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <div class="panel-body">
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <!-- Adding / Editing -->
                            @php
                                $dataTypeRows = $dataType->{($edit ? 'editRows' : 'addRows' )};
                            @endphp

                            @foreach($dataTypeRows as $row)
                                <!-- GET THE DISPLAY OPTIONS -->
                                @php
                                    $display_options = $row->details->display ?? NULL;
                                    if ($dataTypeContent->{$row->field.'_'.($edit ? 'edit' : 'add')}) {
                                        $dataTypeContent->{$row->field} = $dataTypeContent->{$row->field.'_'.($edit ? 'edit' : 'add')};
                                    }
                                @endphp
                                @if (isset($row->details->legend) && isset($row->details->legend->text))
                                    <legend class="text-{{ $row->details->legend->align ?? 'center' }}" style="background-color: {{ $row->details->legend->bgcolor ?? '#f0f0f0' }};padding: 5px;">{{ $row->details->legend->text }}</legend>
                                @endif

                                @if($row->field == "content")
                                    <div id="list_contract_parties">
                                        @foreach($contractParties as $party)
                                            <div class="col-md-12 padding-remove wrapper_contract_party" data-count="{{ $party['id'] }}">
                                                <div class="form-group col-md-11">
                                                    <label class="control-label" for="contract_parties_{{ $party['id'] }}">Partes</label>
                                                    <select class="form-control select_contract_party" name="contract_party[]" id="contract_party_{{ $party['id'] }}">
                                                        <option value="{{ $party['id'] }}" selected> {{ $party['text'] }}</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-1">
                                                    <button type="button" class="btn-plus add_contract_party hidden">
                                                        <span class="voyager-plus"></span>
                                                    </button>
                                                    <button type="button" class="btn-close del_contract_party">
                                                        <span class="voyager-trash"></span>
                                                    </button>
                                                </div>
                                            </div>
                                        @endforeach
                                        @php
                                            $contract_party_count = empty($contractParties) ? 1 : ++end($contractParties)['id'];
                                        @endphp
                                        <div class="col-md-12 padding-remove wrapper_contract_party" data-count="{{ $contract_party_count }}">
                                            <div class="form-group col-md-11">
                                                <label class="control-label" for="contract_parties_{{ $contract_party_count }}">Partes</label>
                                                <select class="form-control select_contract_party" name="contract_party[]" id="contract_party_{{ $contract_party_count }}">
                                                </select>
                                            </div>
                                            <div class="col-md-1">
                                                <button type="button" class="btn-plus add_contract_party">
                                                    <span class="voyager-plus"></span>
                                                </button>
                                                <button type="button" class="btn-close del_contract_party hidden">
                                                    <span class="voyager-trash"></span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                @endif

                                @if($row->field == "content")
                                    <div class="form-group col-md-12">
                                        <label class="control-label" for="custom_variable">Variáveis Personalizadas</label>
                                        <select class="form-control" name="custom_variable" id="custom_variable">
                                            <option value=""></option>
                                            @foreach($customVariables as $key => $party)
                                                @php ++$key @endphp
                                                <option value="cliente_{{ $key }}_nome" data-id="{{ $key }}">Nome - Cliente "{{ $party['text'] }}"</option>
                                                <option value="cliente_{{ $key }}_razao_social" data-id="{{ $key }}">Razão Social - Cliente "{{ $party['text'] }}"</option>
                                                <option value="cliente_{{ $key }}_cnpj_cpf" data-id="{{ $key }}">CNPJ/CPF - Cliente "{{ $party['text'] }}"</option>
                                                <option value="cliente_{{ $key }}_rg" data-id="{{ $key }}">RG - Cliente "{{ $party['text'] }}"</option>
                                                <option value="cliente_{{ $key }}_ie" data-id="{{ $key }}">IE - Cliente "{{ $party['text'] }}"</option>
                                                <option value="cliente_{{ $key }}_proprietario" data-id="{{ $key }}">Proprietário - Cliente "{{ $party['text'] }}"</option>
                                                <option value="cliente_{{ $key }}_data_nascimento" data-id="{{ $key }}">Nascimento - Cliente "{{ $party['text'] }}"</option>
                                                <option value="cliente_{{ $key }}_email" data-id="{{ $key }}">Email - Cliente "{{ $party['text'] }}"</option>
                                                <option value="cliente_{{ $key }}_telefone_1" data-id="{{ $key }}">Telefone 1 - Cliente "{{ $party['text'] }}"</option>
                                                <option value="cliente_{{ $key }}_telefone_2" data-id="{{ $key }}">Telefone 2 - Cliente "{{ $party['text'] }}"</option>
                                                <option value="cliente_{{ $key }}_endereco_rua" data-id="{{ $key }}">Endereço Rua - Cliente "{{ $party['text'] }}"</option>
                                                <option value="cliente_{{ $key }}_endereco_n" data-id="{{ $key }}">Endereço Nº - Cliente "{{ $party['text'] }}"</option>
                                                <option value="cliente_{{ $key }}_bairro" data-id="{{ $key }}">Bairro - Cliente "{{ $party['text'] }}"</option>
                                                <option value="cliente_{{ $key }}_cep" data-id="{{ $key }}">CEP - Cliente "{{ $party['text'] }}"</option>
                                                <option value="cliente_{{ $key }}_cidade" data-id="{{ $key }}">Cidade - Cliente "{{ $party['text'] }}"</option>
                                                <option value="cliente_{{ $key }}_uf" data-id="{{ $key }}">UF - Cliente "{{ $party['text'] }}"</option>
                                            @endforeach
                                        </select>
                                    </div>
                                @endif

                                <div class="form-group @if($row->type == 'hidden') hidden @endif col-md-{{ $display_options->width ?? 12 }} {{ $errors->has($row->field) ? 'has-error' : '' }}" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                                    {{ $row->slugify }}
                                    <label class="control-label" for="name">
                                        {{ $row->getTranslatedAttribute('display_name') }}
                                    </label>
                                    @include('voyager::multilingual.input-hidden-bread-edit-add')
                                    @if (isset($row->details->view))
                                        @include($row->details->view, ['row' => $row, 'dataType' => $dataType, 'dataTypeContent' => $dataTypeContent, 'content' => $dataTypeContent->{$row->field}, 'action' => ($edit ? 'edit' : 'add'), 'view' => ($edit ? 'edit' : 'add'), 'options' => $row->details])
                                    @elseif ($row->type == 'relationship')
                                        @include('voyager::formfields.relationship', ['options' => $row->details])
                                    @else
                                        {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                                    @endif

                                    @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                        {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                                    @endforeach
                                    @if ($errors->has($row->field))
                                        @foreach ($errors->get($row->field) as $error)
                                            <span class="help-block">{{ $error }}</span>
                                        @endforeach
                                    @endif
                                </div>
                            @endforeach
                        </div><!-- panel-body -->

                        <div class="panel-footer">
                            @section('submit-buttons')
                                <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                            @stop
                            @yield('submit-buttons')
                            @if($add)
                                <div class="inline d-auto" style="margin-left: 50px">
                                    <span>Enviar por e-mail:</span>
                                    <ul class="radio-inline">
                                        <li>
                                            <input type="radio" id="option-1"
                                                   name="sendmail"
                                                   value="1">
                                            <label for="option-1">Sim</label>
                                            <div class="check"></div>
                                        </li>
                                        <li>
                                            <input type="radio" id="option-2"
                                                   name="sendmail"
                                                   value="0" checked>
                                            <label for="option-2">Não</label>
                                            <div class="check"></div>
                                        </li>
                                    </ul>
                                </div>
                            @endif
                        </div>
                    </form>

                    <iframe id="form_target" name="form_target" style="display:none"></iframe>
                    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
                            enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                        <input name="image" id="upload_file" type="file"
                                 onchange="$('#my_form').submit();this.value='';">
                        <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
                        {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- templates mustache -->
    <script id="contract_party_template" type="x-tmpl-mustache">
        <div class="col-md-12 padding-remove wrapper_contract_party" data-count="@{{ count }}">
            <div class="form-group col-md-11">
                <label class="control-label" for="contract_party_@{{ count }}">Partes</label>
                <select class="form-control select_contract_party" name="contract_party[]" id="contract_party_@{{ count }}">
                    <option value="">{{__('voyager::generic.none')}}</option>
                </select>
            </div>
            <div class="col-md-1">
                <button type="button" class="btn-plus add_contract_party">
                    <span class="voyager-plus"></span>
                </button>
                <button type="button" class="btn-close del_contract_party hidden">
                    <span class="voyager-trash"></span>
                </button>
            </div>
        </div>
    </script>
    <!-- templates mustache -->

    <!-- End Delete File Modal -->
    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

@section('javascript')
    <script>
        var params = {};
        var $file;

        function deleteHandler(tag, isMulti) {
          return function() {
            $file = $(this).siblings(tag);

            params = {
                slug:   '{{ $dataType->slug }}',
                filename:  $file.data('file-name'),
                id:     $file.data('id'),
                field:  $file.parent().data('field-name'),
                multi: isMulti,
                _token: '{{ csrf_token() }}'
            }

            $('.confirm_delete_name').text(params.filename);
            $('#confirm_delete_modal').modal('show');
          };
        }

        function addOptionsCustomVariable(customer_id, customer_name) {
            let options = [
                new Option('Nome - Cliente "' + customer_name + '"', 'cliente_' + customer_id + '_nome'),
                new Option('Razão Social - Cliente "' + customer_name + '"', 'cliente_' + customer_id + '_razao_social'),
                new Option('CNPJ/CPF - Cliente "' + customer_name + '"', 'cliente_' + customer_id + '_cnpj_cpf'),
                new Option('RG - Cliente "' + customer_name + '"', 'cliente_' + customer_id + '_rg'),
                new Option('IE - Cliente "' + customer_name + '"', 'cliente_' + customer_id + '_ie'),
                new Option('Proprietário - Cliente "' + customer_name + '"', 'cliente_' + customer_id + '_proprietario'),
                new Option('Nascimento - Cliente "' + customer_name + '"', 'cliente_' + customer_id + '_data_nascimento'),
                new Option('Email - Cliente "' + customer_name + '"', 'cliente_' + customer_id + '__email'),
                new Option('Telefone 1 - Cliente "' + customer_name + '"', 'cliente_' + customer_id + '_telefone_1'),
                new Option('Telefone 2 - Cliente "' + customer_name + '"', 'cliente_' + customer_id + '_telefone_2'),
                new Option('Endereço Rua - Cliente "' + customer_name + '"', 'cliente_' + customer_id + '_endereco_rua'),
                new Option('Endereço Nº - Cliente "' + customer_name + '"', 'cliente_' + customer_id + '_endereco_n'),
                new Option('Bairro - Cliente "' + customer_name + '"', 'cliente_' + customer_id + '_bairro'),
                new Option('CEP - Cliente "' + customer_name + '"', 'cliente_' + customer_id + '_cep'),
                new Option('Cidade - Cliente "' + customer_name + '"', 'cliente_' + customer_id + '_cidade'),
                new Option('UF - Cliente "' + customer_name + '"', 'cliente_' + customer_id + '_uf'),
            ];

            options.forEach(function (element) {
                $("#custom_variable").append(element);
            });

            $("#custom_variable").trigger('change');
        }
        function removeOptionsCustomVariable(customer_id) {
            $("#custom_variable option[value^='cliente_" + customer_id + "']").remove();
        }
        function removeCustomVariableFromText(customer_id) {
            let content = tinymce.activeEditor.getContent();
            let regex = [
                new RegExp('\{\{( )?cliente_'+ customer_id +'_nome( )?\}\}','g'),
                new RegExp('\{\{( )?cliente_'+ customer_id +'_razao_social( )?\}\}','g'),
                new RegExp('\{\{( )?cliente_'+ customer_id +'_cnpj_cpf( )?\}\}','g'),
                new RegExp('\{\{( )?cliente_'+ customer_id +'_rg( )?\}\}','g'),
                new RegExp('\{\{( )?cliente_'+ customer_id +'_ie( )?\}\}','g'),
                new RegExp('\{\{( )?cliente_'+ customer_id +'_proprietario( )?\}\}','g'),
                new RegExp('\{\{( )?cliente_'+ customer_id +'_data_nascimento( )?\}\}','g'),
                new RegExp('\{\{( )?cliente_'+ customer_id +'_telefone_1( )?\}\}','g'),
                new RegExp('\{\{( )?cliente_'+ customer_id +'_telefone_2( )?\}\}','g'),
                new RegExp('\{\{( )?cliente_'+ customer_id +'_endereco_rua( )?\}\}','g'),
                new RegExp('\{\{( )?cliente_'+ customer_id +'_endereco_n( )?\}\}','g'),
                new RegExp('\{\{( )?cliente_'+ customer_id +'_bairro( )?\}\}','g'),
                new RegExp('\{\{( )?cliente_'+ customer_id +'_cep( )?\}\}','g'),
                new RegExp('\{\{( )?cliente_'+ customer_id +'_cidade( )?\}\}','g'),
                new RegExp('\{\{( )?cliente_'+ customer_id +'_uf( )?\}\}','g'),
            ];

            regex.forEach(function (reg) {
                content = content.replaceAll(reg,'');
            });

            tinymce.activeEditor.setContent(content);
        }

        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                } else if (elt.type != 'date') {
                    elt.type = 'text';
                    $(elt).datetimepicker({
                        format: 'L',
                        extraFormats: [ 'YYYY-MM-DD' ]
                    }).datetimepicker($(elt).data('datepicker'));
                }
            });

            @if ($isModelTranslatable)
                $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function(i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', deleteHandler('img', true));
            $('.form-group').on('click', '.remove-single-image', deleteHandler('img', false));
            $('.form-group').on('click', '.remove-multi-file', deleteHandler('a', true));
            $('.form-group').on('click', '.remove-single-file', deleteHandler('a', false));

            $('#confirm_delete').on('click', function(){
                $.post('{{ route('voyager.'.$dataType->slug.'.media.remove') }}', params, function (response) {
                    if ( response
                        && response.data
                        && response.data.status
                        && response.data.status == 200 ) {

                        toastr.success(response.data.message);
                        $file.parent().fadeOut(300, function() { $(this).remove(); })
                    } else {
                        toastr.error("Error removing file.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();

            $("#contract_model > select.select2-ajax").change(function (e) {
                let model_id = e.target.value;
                let path_url = "{{ config('app.url') }}";
                path_url += "/contracts-models/json/" + model_id;

                if(_.isEmpty(model_id))
                    return false;

                axios.get(path_url)
                    .then(function (response) {
                        if(response.data.status) {
                            $("#richtextcontent_ifr")
                                .contents()
                                .find("body")
                                .html(response.data.content);
                        }
                    });
            });

            window["customers"] = @json($customers);
            load_select = function ($select) {
                $select.select2({
                    ajax: {
                        transport: function(params, success, failure) {
                            let optionDefault = [{"id": "", "text": "Nenhum"}];
                            let items = [...optionDefault, ...window["customers"]];
                            let promise = new Promise(function(resolve, reject) {
                                resolve({results: items});
                            });
                            promise.then(success);
                            promise.catch(failure);
                        }
                    }
                });
            };
            load_select($(".select_contract_party"));

            $(document).on("click", ".add_contract_party", function () {
                let $wrapper = $(this).parents(".wrapper_contract_party");
                let count = parseInt($wrapper.attr("data-count")) + 1;
                let data = {"count": count};
                let template = $("#contract_party_template").html();
                let contract_party = Mustache.render(template,data);

                $("#list_contract_parties").append(contract_party);
                load_select($("#contract_party_" + count));

                $(this).addClass("hidden")
                    .siblings(".del_contract_party")
                    .removeClass("hidden");

                let customer_id = $wrapper.find('select.select_contract_party').val();
                let customer_name = $wrapper.find('select.select_contract_party option:selected').text();
                addOptionsCustomVariable(customer_id,customer_name);
            });
            $(document).on("click", ".del_contract_party", function () {
                let customer_id = $(this).parents(".wrapper_contract_party").find(".select_contract_party").val();

                _.map(window["customers"], function (element,order) {
                    if(element.id == customer_id) {
                        element.disabled = false;
                        window["customers"][order] = element;
                    }
                });

                $(this).parents(".wrapper_contract_party").remove();
                removeOptionsCustomVariable(customer_id);
                removeCustomVariableFromText(customer_id);
            });

            let oldValue = null;
            let newValue = null;
            $(document).on("select2:selecting", ".select_contract_party", function ( ) {
                oldValue = $(this).val();
            });
            $(document).on("select2:select", ".select_contract_party", function ( ) {
                newValue = $(this).val();

                if(newValue != oldValue) {
                    _.map(window["customers"], function (element,order) {
                        if(element.id == newValue) {
                            element.disabled = true;
                            window["customers"][order] = element;
                        }

                        if(element.id == oldValue) {
                            element.disabled = false;
                            window["customers"][order] = element;
                        }
                    });
                }

                oldValue = null;
                newValue = null;
            });

            $("#custom_variable").select2();
            $(document).on('select2:select', '#custom_variable', function (e) {
                let custom_variable = "\{\{ " + e.target.value + " \}\}";
                tinymce.activeEditor.execCommand('mceInsertContent', false, custom_variable);
                $('#custom_variable').val('').trigger('change');
            });
        });
    </script>
@stop
