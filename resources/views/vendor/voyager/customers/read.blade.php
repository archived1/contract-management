@extends('voyager::master')

@section('page_title', __('voyager::generic.view').' '.$dataType->getTranslatedAttribute('display_name_singular'))

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i> {{ __('voyager::generic.viewing') }} {{ ucfirst($dataType->getTranslatedAttribute('display_name_singular')) }} &nbsp;

        @can('edit', $dataTypeContent)
            <a href="{{ route('voyager.'.$dataType->slug.'.edit', $dataTypeContent->getKey()) }}" class="btn btn-info">
                <span class="glyphicon glyphicon-pencil"></span>&nbsp;
                {{ __('voyager::generic.edit') }}
            </a>
        @endcan
        @can('delete', $dataTypeContent)
            @if($isSoftDeleted)
                <a href="{{ route('voyager.'.$dataType->slug.'.restore', $dataTypeContent->getKey()) }}" title="{{ __('voyager::generic.restore') }}" class="btn btn-default restore" data-id="{{ $dataTypeContent->getKey() }}" id="restore-{{ $dataTypeContent->getKey() }}">
                    <i class="voyager-trash"></i> <span class="hidden-xs hidden-sm">{{ __('voyager::generic.restore') }}</span>
                </a>
            @else
                <a href="javascript:;" title="{{ __('voyager::generic.delete') }}" class="btn btn-danger delete" data-id="{{ $dataTypeContent->getKey() }}" id="delete-{{ $dataTypeContent->getKey() }}">
                    <i class="voyager-trash"></i> <span class="hidden-xs hidden-sm">{{ __('voyager::generic.delete') }}</span>
                </a>
            @endif
        @endcan

        <a href="{{ route('voyager.'.$dataType->slug.'.index') }}" class="btn btn-warning">
            <span class="glyphicon glyphicon-list"></span>&nbsp;
            {{ __('voyager::generic.return_to_list') }}
        </a>
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content read container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="container view-bread">
                    <!-- form start -->
                    @foreach($dataType->readRows as $row)
                        @if ($row->field == 'avatar' || $row->field == 'customer_type' || property_exists($row->details, 'belongToType') && ($row->details->belongToType != $dataTypeContent->customer_type))
                            @continue
                        @endif

                        @php
                            if ($dataTypeContent->{$row->field.'_read'}) {
                                $dataTypeContent->{$row->field} = $dataTypeContent->{$row->field.'_read'};
                            }
                            $display_options = $row->details->display ?? NULL;
                        @endphp
                        <div class="col-md-{{ $display_options->width ?? 12 }}">
                            <div class="panel panel-bordered" style="padding-bottom:5px;">
                                <div class="panel-heading" style="border-bottom:0;">
                                    <h3 class="panel-title">{{ $row->getTranslatedAttribute('display_name') }} </h3>
                                </div>
                                <div class="panel-body" style="padding-top:0;">
                                    @if (isset($row->details->view))
                                        @include($row->details->view, ['row' => $row, 'dataType' => $dataType, 'dataTypeContent' => $dataTypeContent, 'content' => $dataTypeContent->{$row->field}, 'action' => 'read', 'view' => 'read', 'options' => $row->details])
                                    @elseif($row->type == 'relationship')
                                        @include('voyager::formfields.relationship', ['view' => 'read', 'options' => $row->details])
                                    @elseif($row->type == 'select_dropdown' && property_exists($row->details, 'options') &&
                                            !empty($row->details->options->{$dataTypeContent->{$row->field}})
                                    )
                                        <?php echo $row->details->options->{$dataTypeContent->{$row->field}};?>
                                    @elseif($row->type == 'select_multiple')
                                        @if(property_exists($row->details, 'relationship'))

                                            @foreach(json_decode($dataTypeContent->{$row->field}) as $item)
                                                {{ $item->{$row->field}  }}
                                            @endforeach

                                        @elseif(property_exists($row->details, 'options'))
                                            @if (!empty(json_decode($dataTypeContent->{$row->field})))
                                                @foreach(json_decode($dataTypeContent->{$row->field}) as $item)
                                                    @if (@$row->details->options->{$item})
                                                        {{ $row->details->options->{$item} . (!$loop->last ? ', ' : '') }}
                                                    @endif
                                                @endforeach
                                            @else
                                                {{ __('voyager::generic.none') }}
                                            @endif
                                        @endif
                                    @elseif($row->type == 'date' || $row->type == 'timestamp')
                                        @if ( property_exists($row->details, 'format') && !is_null($dataTypeContent->{$row->field}) )
                                            {{ \Carbon\Carbon::parse($dataTypeContent->{$row->field})->formatLocalized($row->details->format) }}
                                        @else
                                            {{ $dataTypeContent->{$row->field} }}
                                        @endif
                                    @elseif($row->type == 'checkbox')
                                        @if(property_exists($row->details, 'on') && property_exists($row->details, 'off'))
                                            @if($dataTypeContent->{$row->field})
                                                <span class="label label-info">{{ $row->details->on }}</span>
                                            @else
                                                <span class="label label-primary">{{ $row->details->off }}</span>
                                            @endif
                                        @else
                                            {{ $dataTypeContent->{$row->field} }}
                                        @endif
                                    @elseif($row->type == 'color')
                                        <span class="badge badge-lg" style="background-color: {{ $dataTypeContent->{$row->field} }}">{{ $dataTypeContent->{$row->field} }}</span>
                                    @elseif($row->type == 'coordinates')
                                        @include('voyager::partials.coordinates')
                                    @elseif($row->type == 'rich_text_box')
                                        @include('voyager::multilingual.input-hidden-bread-read')
                                        {!! $dataTypeContent->{$row->field} !!}
                                    @elseif($row->type == 'file')
                                        @if(json_decode($dataTypeContent->{$row->field}))
                                            @foreach(json_decode($dataTypeContent->{$row->field}) as $file)
                                                <a href="{{ Storage::disk(config('voyager.storage.disk'))->url($file->download_link) ?: '' }}">
                                                    {{ $file->original_name ?: '' }}
                                                </a>
                                                <br/>
                                            @endforeach
                                        @else
                                            <a href="{{ Storage::disk(config('voyager.storage.disk'))->url($row->field) ?: '' }}">
                                                {{ __('voyager::generic.download') }}
                                            </a>
                                        @endif
                                    @else
                                        @include('voyager::multilingual.input-hidden-bread-read')
                                        <p>{{ $dataTypeContent->{$row->field} }} &nbsp;</p>
                                    @endif
                                </div>
                                @if(!$loop->last)
                                    <hr style="margin:0;">
                                @endif
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <div class="page-content browse container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h3 class="page-title" style="height: auto;">Lista de documentos</h3>
                <div class="container">
                    <div class="panel panel-bordered">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table id="dataTable" class="table table-hover">
                                    <thead>
                                        <th>ID</th>
                                        <th>Tipo de Documento</th>
                                        <th>Modelo de Documento</th>
                                        <th>Cliente</th>
                                        <th class="actions text-right dt-not-orderable">Ações</th>
                                    </thead>
                                    <tbody>
                                        @php
                                            $documents = $dataTypeContent->documents;
                                        @endphp
                                        @foreach($documents as $document)
                                            <tr>
                                                <td>
                                                    {{ $document->id }}
                                                </td>
                                                <td>
                                                    {{!empty($document->contractType) ? $document->contractType->name : null}}
                                                </td>
                                                <td>
                                                    {{!empty($document->contractModel) ? $document->contractModel->name : null}}
                                                </td>
                                                <td>
                                                    {{!empty($document->customer) ? $document->customer->name : null}}
                                                </td>
                                                <td class="no-sort no-click bread-actions">
                                                    <a href="{{ route('voyager.documents.show',['id' => $document->id]) }}" title="Ver" class="btn btn-sm btn-warning pull-right view">
                                                        <i class="voyager-eye"></i> <span class="hidden-xs hidden-sm">Ver</span>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="page-content browse container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h3 class="page-title" style="height: auto;">Lista de uploads</h3>
                <div class="container">
                    <div class="panel panel-bordered">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table id="dataTable-Uploads" class="table table-hover">
                                    <thead>
                                    <th>ID</th>
                                    <th>Tipo de Documento</th>
                                    <th>Nome</th>
                                    <th class="actions text-right dt-not-orderable">Ações</th>
                                    </thead>
                                    <tbody>
                                        @php
                                            $uploads = $dataTypeContent->uploads;
                                        @endphp
                                        @foreach($uploads as $upload)
                                            <tr>
                                                <td>
                                                    {{ $upload->id }}
                                                </td>
                                                <td>
                                                    {{!empty($upload->contractType) ? $upload->contractType->name : null}}
                                                </td>
                                                <td>
                                                    {{!empty($upload->name) ? $upload->name : null}}
                                                </td>
                                                <td class="no-sort no-click bread-actions">
                                                    <a href="{{ route('customer.delete_upload',['document' => $upload->id]) }}" target="_blank" title="Delete" class="btn btn-sm btn-danger pull-right view">
                                                        <i class="voyager-download"></i> <span class="hidden-xs hidden-sm">Remover</span>
                                                    </a>
                                                    <a href="{{ route('customer.download',['download' => $upload->id]) }}" target="_blank" title="Download" class="btn btn-sm btn-success pull-right view">
                                                        <i class="voyager-trash"></i> <span class="hidden-xs hidden-sm">Download</span>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="container">
                    <div class="panel panel-bordered">
                        <div class="panel-body">
                            <form action="{{ route('customer.upload', ['customer' => $dataTypeContent->id]) }}" method="post" enctype="multipart/form-data">
                                <div class="form-group col-sm-12 col-md-6">
                                    <label class="control-label" for="name">Nome</label>
                                    <input type="text" class="form-control" name="document_name" required>
                                </div>
                                <div class="form-group col-sm-12 col-md-6">
                                    <label class="control-label" for="document_type">Tipo de Documento</label>
                                    <select class="form-control select2" name="document_type" required>
                                        <option value="" selected disabled></option>
                                        @foreach($documentTypers as $documentType)
                                            <option value="{{ $documentType->id }}" >{{ $documentType->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <input name="document_file" type="file" required>
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-primary">Salvar</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Single delete modal --}}
    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }} {{ strtolower($dataType->getTranslatedAttribute('display_name_singular')) }}?</h4>
                </div>
                <div class="modal-footer">
                    <form action="{{ route('voyager.'.$dataType->slug.'.index') }}" id="delete_form" method="POST">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger pull-right delete-confirm"
                               value="{{ __('voyager::generic.delete_confirm') }} {{ strtolower($dataType->getTranslatedAttribute('display_name_singular')) }}">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop

@section('javascript')
    @if ($isModelTranslatable)
        <script>
            $(document).ready(function () {
                $('.side-body').multilingual();
            });
        </script>
    @endif
    <script>
        var deleteFormAction;
        $('.delete').on('click', function (e) {
            var form = $('#delete_form')[0];

            if (!deleteFormAction) {
                // Save form action initial value
                deleteFormAction = form.action;
            }

            form.action = deleteFormAction.match(/\/[0-9]+$/)
                ? deleteFormAction.replace(/([0-9]+$)/, $(this).data('id'))
                : deleteFormAction + '/' + $(this).data('id');

            $('#delete_modal').modal('show');
        });

        $(document).ready(function () {
            var table = $('#dataTable').DataTable({!! json_encode(
                    array_merge([
                        "order" => [],
                        "language" => __('voyager::datatable'),
                        "columnDefs" => [
                            ['targets' => 'dt-not-orderable', 'searchable' =>  false, 'orderable' => false],
                        ],
                    ],
                    config('voyager.dashboard.data_tables', []))
                , true)
            !!});

            var tableUploads = $('#dataTable-Uploads').DataTable({!! json_encode(
                    array_merge([
                        "order" => [],
                        "language" => __('voyager::datatable'),
                        "columnDefs" => [
                            ['targets' => 'dt-not-orderable', 'searchable' =>  false, 'orderable' => false],
                        ],
                    ],
                    config('voyager.dashboard.data_tables', []))
                , true)
            !!});
        });
    </script>
@stop
