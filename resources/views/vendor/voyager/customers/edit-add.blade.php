@php
    $edit = !is_null($dataTypeContent->getKey());
    $add  = is_null($dataTypeContent->getKey());
@endphp

@extends('voyager::master')

@section('page_title', __('voyager::generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular'))

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular') }}
    </h1>
@stop

@section('content')
    <div class="page-content container-fluid">
        <form class="form-edit-add" role="form"
              action="{{ $edit ? route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) : route('voyager.'.$dataType->slug.'.store') }}"
              method="POST" enctype="multipart/form-data" autocomplete="off">
            <!-- PUT Method if we are editing -->
            @if($edit)
                {{ method_field("PUT") }}
            @endif
            {{ csrf_field() }}

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-bordered">
                    {{-- <div class="panel"> --}}
                        @if(count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @php
                            $dataTypeRows = $dataType->{($edit ? 'editRows' : 'addRows' )};
                            $customer = $dataTypeRows->where('field','customer_type')->first();
                            $customer_type = !empty($dataTypeContent->customer_type)
                                                ? $dataTypeContent->customer_type
                                                : (isset($customer->details->default)
                                                    ? $customer->details->default
                                                    : 1
                                                  );
                        @endphp
                        @if(!empty($customer))
                            <input type="hidden" name="{{ $customer->field }}" id="customer-type" value="{{$customer_type}}">
                        @endif
                        <div class="panel-body">
                            <!-- Adding / Editing -->
                            @foreach($dataTypeRows as $row)
                                @if($row->field == 'avatar' || $row->field == 'customer_type')
                                    @continue
                                @endif

                                <!-- GET THE DISPLAY OPTIONS -->
                                @php
                                    $display_options = $row->details->display ?? NULL;
                                    if ($dataTypeContent->{$row->field.'_'.($edit ? 'edit' : 'add')}) {
                                        $dataTypeContent->{$row->field} = $dataTypeContent->{$row->field.'_'.($edit ? 'edit' : 'add')};
                                    }
                                @endphp

                                @if(isset($row->details->legend) && isset($row->details->legend->text))
                                    <legend class="text-{{ $row->details->legend->align ?? 'center' }}" style="background-color: {{ $row->details->legend->bgcolor ?? '#f0f0f0' }};padding: 5px;">{{ $row->details->legend->text }}</legend>
                                @endif

                                <div class="form-group @if($row->type == 'hidden' || (property_exists($row->details, 'belongToType') && $row->details->belongToType != $customer_type)) hidden @endif col-md-{{ $display_options->width ?? 12 }} {{ $errors->has($row->field) ? 'has-error' : '' }} field-{{ $row->field }}" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                                    {{ $row->slugify }}
                                    <label class="control-label" for="name">{{ $row->getTranslatedAttribute('display_name') }}</label>
                                    @include('voyager::multilingual.input-hidden-bread-edit-add')
                                    @if(isset($row->details->view))
                                        @include($row->details->view, ['row' => $row, 'dataType' => $dataType, 'dataTypeContent' => $dataTypeContent, 'content' => $dataTypeContent->{$row->field}, 'action' => ($edit ? 'edit' : 'add'), 'view' => ($edit ? 'edit' : 'add'), 'options' => $row->details])
                                    @elseif ($row->type == 'relationship')
                                        @include('voyager::formfields.relationship', ['options' => $row->details])
                                    @else
                                        {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                                    @endif

                                    @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                        {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                                    @endforeach

                                    @if($errors->has($row->field))
                                        @foreach ($errors->get($row->field) as $error)
                                            <span class="help-block">{{ $error }}</span>
                                        @endforeach
                                    @endif
                                </div>
                            @endforeach
                            <button type="submit" class="btn btn-primary save">
                                {{ __('voyager::generic.save') }}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <iframe id="form_target" name="form_target" style="display:none"></iframe>
        <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post" enctype="multipart/form-data" style="width:0px;height:0;overflow:hidden">
            {{ csrf_field() }}
            <input name="image" id="upload_file" type="file" onchange="$('#my_form').submit();this.value='';">
            <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
        </form>
    </div>
@stop

@section('javascript')
    <script>
        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            let customer = {
                "init": function () {
                    $(document).on('blur','#cpf_cnpj input', function () {
                        customer.update();
                    });
                },
                "current_type": function () {
                    var customer_type = ($("#cpf_cnpj input").val()).length > 14 ? 2 : 1;
                    var input = $("#customer-type").val(customer_type);
                    return input.val();
                },
                "get_inputs_from_type": function (type_id) {
                    if(type_id == 1)
                        return ".field-company, .field-ie, .field-owner";
                    else if(type_id == 2)
                        return ".field-rg, .field-date_of_birth";
                    else
                        return null;
                },
                "update": function () {
                    _.delay(function () {
                        let customer_type = customer.current_type();
                        let selects_inputs = customer.get_inputs_from_type(customer_type);
                        let field_name = customer_type == 2 ? 'Nome Fantasia' : 'Nome';
                        $(".field-name .control-label").text(field_name);
                        $(".field-name input.form-control").attr('placeholder',field_name);
                        $("[class*='field-']").removeClass('hidden').attr('disabled', false);
                        $(selects_inputs).addClass('hidden').attr('disabled', true);
                    },50);
                }
            };
            customer.init();

            $('input.cep').blur(function (e) {
                let cep = e.target.value.replace('-','');

                $('#voyager-loader').fadeIn();
                $.ajax({
                    url: "{{route('api.address.cep')}}",
                    type: 'GET',
                    data: {"cep": cep},
                    dataType: 'json',
                    success: function(data) {
                        if(data.status) {
                            $("input[name='address_street']").val(data.address.logradouro);
                            $("input[name='address_complement']").val(data.address.complemento);
                            $("input[name='district']").val(data.address.bairro);
                            $("input[name='city']").val(data.address.localidade);
                            $("#list_uf > select").val(data.address.uf).trigger('change');
                        }
                    }
                }).done(function () {
                    $('#voyager-loader').fadeOut();
                });
            })
        });
    </script>
@stop
