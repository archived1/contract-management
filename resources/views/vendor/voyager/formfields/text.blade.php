<input @if($row->required == 1) required @endif type="{{ isset($options->type) ? $options->type : 'text' }}" class="form-control {{ property_exists($options,'class') ? $options->class : null }}"
       name="{{ $row->field }}" placeholder="{{ old($row->field, $options->placeholder ?? $row->getTranslatedAttribute('display_name')) }}"
       {!! isBreadSlugAutoGenerator($options) !!}
       value="{{ old($row->field, $dataTypeContent->{$row->field} ?? $options->default ?? '') }}">
