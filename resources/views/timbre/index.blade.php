@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', 'Timbre de Documentos')

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-file-text"></i>
        Timbre de Documentos
    </h1>
@stop

@section('content')
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <form action="{{ route('timbre.update') }}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="panel-heading">
                                <h3 class="panel-title">
                                    Timbre de Cabeçalho
                                </h3>
                                <div class="panel-actions">
                                    <button type="submit" class="btn btn-success">{{ __('voyager::generic.save') }}</button>
                                </div>
                            </div>
                            <div class="panel-body">
                                <input type="file" name="timbre_header" accept="image/*" class="col-sm-12" required>
                                @if(file_exists(public_path('storage/timbre/timbre-header.png')))
                                    <div style="display: block; width: 100%; position: relative;">
                                        <img src="{{ asset("storage/timbre/timbre-header.png") }}"
                                             style="display: table; width: auto; height: 100px; clear: both; margin: auto; padding:2px; border:1px solid #ddd; margin-bottom:10px;">
                                    </div>
                                @endif
                            </div>
                        </form>

                        <form action="{{ route('timbre.update') }}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="panel-heading">
                                <h3 class="panel-title">
                                    Timbre de Rodapé
                                </h3>
                                <div class="panel-actions">
                                    <button type="submit" class="btn btn-success">{{ __('voyager::generic.save') }}</button>
                                </div>
                            </div>
                            <div class="panel-body">
                                <input type="file" name="timbre_footer" accept="image/*" class="col-sm-12" required>
                                @if(file_exists(public_path('storage/timbre/timbre-footer.png')))
                                    <div>
                                        <img src="{{ asset("storage/timbre/timbre-footer.png") }}"
                                             style="display: table; width: auto; height: 100px; clear: both; margin: auto; padding:2px; border:1px solid #ddd; margin-bottom:10px;">
                                    </div>
                                @endif
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        $(function () {

        });
    </script>
@stop
