@if(file_exists(public_path('storage/timbre/timbre-footer.png')))
    <div style="width: 100%; height: 100px; position: absolute; top: auto; bottom: 0px;">
        <div style="display: block; width: 100%; height: 100px; position: relative; text-align: center;">
            <img src="./storage/timbre/timbre-footer.png" style="width: auto; height: 100px;">
        </div>
    </div>
@endif
