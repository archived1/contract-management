<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ setting('admin.name') }}</title>
    <!-- Fonts -->
    <!-- Styles -->
</head>
<body>
    <div class="flex-center position-ref full-height" id="wrapper-pdf">
        {!! $timbre_header !!}
        {!! $content !!}
        {!! $timbre_footer !!}
    </div>
</body>
</html>
