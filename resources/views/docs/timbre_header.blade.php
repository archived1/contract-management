@if(file_exists(public_path('storage/timbre/timbre-header.png')))
    <div id="timbre-header">
        <div style="display: block; width: 100%; height: 100px; position: relative; text-align: center;">
            <img src="./storage/timbre/timbre-header.png" style="width: auto; height: 100px;">
        </div>
    </div>
@endif
