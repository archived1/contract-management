@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager::generic.reports'))

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-file-text"></i>
        {{ __('voyager::generic.reports') }}
    </h1>
@stop

@section('content')
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <form id="form_report" action="{{ route('reports.export') }}" method="post">
                            {{ csrf_field() }}
                            <div class="form-group col-sm-12 col-md-6 col-md-offset-3">
                                <label class="control-label" for="report_type">Tipo de Relatório</label>
                                <select class="form-control" name="report_type" id="select_report_type" required>
                                    <option value="" selected disabled></option>
                                    <option value="1">Documentos emitidos por período</option>
                                    <option value="2">Documentos emitidos por usuário</option>
                                    <option value="3">Documentos emitidos por cliente</option>
                                    <option value="4">Documentos baixados por período</option>
                                    <option value="5">Documentos baixados por cliente</option>
                                    <option value="6">Aniversariantes por mês</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6 input-all input-date">
                                <label class="control-label" for="date_start">Data Inicial</label>
                                <input type="date" class="form-control" name="date_start" id="input_date_start" max="{{ now()->toDateString('Y-m-d') }}">
                            </div>
                            <div class="form-group col-md-6 input-all input-date">
                                <label class="control-label" for="date_end">Data Final</label>
                                <input type="date" class="form-control" name="date_end" id="input_date_end" max="{{ now()->toDateString('Y-m-d') }}">
                            </div>
                            <div class="form-group col-md-6 col-md-offset-3 input-all input-date-month">
                                <label class="control-label" for="date_end">Mês</label>
                                <select class="form-control selct2" name="month" id="select_date_month">
                                    <option value="01">Janeiro</option>
                                    <option value="02">Fevereiro</option>
                                    <option value="03">Março</option>
                                    <option value="04">Abril</option>
                                    <option value="05">Maio</option>
                                    <option value="06">Junho</option>
                                    <option value="07">Julho</option>
                                    <option value="08">Agosto</option>
                                    <option value="09">Setembro</option>
                                    <option value="10">Outubro</option>
                                    <option value="11">Novembro</option>
                                    <option value="12">Dezembro</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6 col-md-offset-3 input-all input-user">
                                <label class="control-label" for="user">Usuário</label>
                                <select class="form-control select2" name="user" id="select_user">
                                    <option value="0" selected>Selecione um usuário</option>
                                    @foreach($users as $user)
                                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-6 col-md-offset-3 input-all input-customer">
                                <label class="control-label" for="customer">Cliente</label>
                                <select class="form-control select2" name="customer" id="select_customer">
                                    <option value="0" selected>Selecione um cliente</option>
                                    @foreach($customers as $customer)
                                        <option value="{{ $customer->id }}">{{ $customer->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-success pull-right">Gerar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
<script>
    $(function () {
        $("#select_report_type").select2({
            placeholder: "Selecione o tipo de Relatório",
            allowClear: true
        });

        var input_disable = function($select) {
            $select
                .hide()
                .find(':input')
                .prop("disabled", true)
                .attr('required', false);
        };
        var input_enable = function($select) {
            $select
                .show()
                .find(':input')
                .prop('disabled', false)
                .attr('required', true);
        };

        input_disable($('.input-all'));
        $("#select_report_type").on('change', function (e) {
            var report_type = e.target.value;
            switch (report_type) {
                case '1':
                    input_disable($('.input-all'));
                    input_enable($('.input-date'));
                    break;
                case '2':
                    input_disable($('.input-all'));
                    input_enable($('.input-user'));
                    break;
                case '3':
                    input_disable($('.input-all'));
                    input_enable($('.input-customer'));
                    break;
                case '4':
                    input_disable($('.input-all'));
                    input_enable($('.input-date'));
                    break;
                case '5':
                    input_disable($('.input-all'));
                    input_enable($('.input-customer'));
                    break;
                case '6':
                    input_disable($('.input-all'));
                    input_enable($('.input-date-month'));
                    break;
            }
        });
        $("#form_report").submit(function (e) {
            var data = $('#form_report').serializeArray();
            var abort = false;
            data.forEach(function (input) {
                if(input.name == 'customer' && input.value == 0) {
                    $("#select_customer").select2('open');
                    abort = true;
                }

                if(input.name == 'user' && input.value == 0) {
                    $("#select_user").select2('open');
                    abort = true;
                }
            });
            if(abort)
                return false;
        });

        $("#input_date_start").on('change', function (e) {
            $("#input_date_end").attr('min',e.target.value);
        });
        $("#input_date_end").on('change', function (e) {
            $("#input_date_start").attr('max',e.target.value);
        });
    });
</script>
@stop
