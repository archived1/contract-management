const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/assets/js')
    .sass('resources/sass/app.scss', 'public/assets/css')
    .sass('resources/sass/admin/app.scss', 'public/assets/css/admin')
    .copy('node_modules/jquery-mask-plugin/dist', 'public/assets/vendor/jquery-mask')
    .copy('node_modules/jquery/dist', 'public/assets/vendor/jquery')
    .copy('node_modules/mustache/mustache.min.js', 'public/assets/vendor/mustache');
